package com.example.demo.component;

import com.example.demo.entity.User;
import com.example.demo.entity.enums.Role;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String ddlMode;

    @Value("${app.admin.username}")
    private String adminUsername;

    @Value("${app.admin.password}")
    private String adminPassword;

    @Value("${app.admin.active}")
    private boolean adminActive;

    @Value("${app.admin.role}")
    private Role role;

    @Override
    public void run(String... args) throws Exception {
        if (Objects.equals(ddlMode, "create")) {
            User admin = new User(
                    adminUsername,
                    passwordEncoder.encode(adminPassword));
            admin.setRole(role);
            admin.setActive(adminActive);
            userRepository.save(admin);
        }
    }
}
