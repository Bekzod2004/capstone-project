package com.example.demo.exceptions;

import com.example.demo.apiResult.ApiResult;
import com.example.demo.apiResult.Error;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {

    @ExceptionHandler(value = RestException.class)
    public ResponseEntity<ApiResult<List<Error>>> exceptionHandle(RestException ex) {
        ApiResult<List<Error>> result =
                ApiResult.fail(ex.getMessage(),
                        ex.getStatus().value());
        return new ResponseEntity<>(result, ex.getStatus());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ApiResult<List<Error>>> exceptionHandle(
            MethodArgumentNotValidException ex) {
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();

        List<Error> errorDataList = new ArrayList<>();

        for (FieldError fieldError : fieldErrors)
            errorDataList.add(
                    new Error(fieldError.getDefaultMessage(),
                            HttpStatus.BAD_REQUEST.value()));

        ApiResult<List<Error>> apiResult = ApiResult.fail(errorDataList);
        return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = EmptyResultDataAccessException.class)
    public ResponseEntity<ApiResult<List<Error>>> exceptionHandle(EmptyResultDataAccessException ex) {
        ApiResult<List<Error>> result =
                ApiResult.fail(ex.getMessage(),
                        HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<ApiResult<List<Error>>> exceptionHandle(AccessDeniedException ex) {
        ApiResult<List<Error>> apiResult = ApiResult.fail(
                "Huquqingiz yo'q okasi ",
                HttpStatus.FORBIDDEN.value());
        return new ResponseEntity<>(apiResult, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = InsufficientAuthenticationException.class)
    public ResponseEntity<ApiResult<List<Error>>> exceptionHandle(InsufficientAuthenticationException ex) {
        ApiResult<List<Error>> apiResult = ApiResult.fail(
                "Full authentication is required to access this resource",
                HttpStatus.UNAUTHORIZED.value());
        return new ResponseEntity<>(apiResult, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ApiResult<List<Error>>> exceptionHandle(Exception ex) {
        System.out.println(Thread.currentThread().getName());
        log.error("Exception: ",ex);
        ApiResult<List<Error>> apiResult = ApiResult.fail(
                ex.getMessage(),
                HttpStatus.CONFLICT.value());
        return new ResponseEntity<>(apiResult, HttpStatus.CONFLICT);
    }
    @ExceptionHandler(value = AccountStatusException.class)
    public ResponseEntity<ApiResult<List<Error>>> exceptionHandle(AccountStatusException ex) {
        log.error("Exception: ",ex);
        ApiResult<List<Error>> apiResult = ApiResult.fail(
                ex.getMessage(),
                HttpStatus.UNAUTHORIZED.value());
        return new ResponseEntity<>(apiResult, HttpStatus.UNAUTHORIZED);
    }


}
