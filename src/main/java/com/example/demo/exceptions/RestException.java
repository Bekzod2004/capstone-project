package com.example.demo.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter

public class RestException extends RuntimeException{
    private final String message;

    private final HttpStatus status;

    public RestException(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    public static RestException restThrow(String format, HttpStatus unauthorized) {
        return new RestException(format,unauthorized);
    }
}
