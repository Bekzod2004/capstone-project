package com.example.demo.service;

import com.example.demo.DTO.AddBookDTO;
import com.example.demo.DTO.BookDTO;
import com.example.demo.apiResult.ApiResult;
import com.example.demo.entity.Book;
import com.example.demo.exceptions.RestException;
import com.example.demo.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Override
    public ApiResult<BookDTO> getById(Integer id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> RestException.restThrow("No User found with this id = " + id, HttpStatus.NOT_FOUND));
        return ApiResult.success(BookDTO.DTO(book));
    }

    @Override
    public ApiResult<List<BookDTO>> getAll() {
        return ApiResult.success(bookRepository.findAll().stream().map(BookDTO::DTO).collect(Collectors.toList()));
    }

    @Override
    public ApiResult<BookDTO> addBook(AddBookDTO bookDTO) {
        Book book = new Book();
        book.setName(bookDTO.getName());
        book.setAuthor(bookDTO.getAuthor());
        book.setPageCount(bookDTO.getPageCount());
        return ApiResult.success(BookDTO.DTO(bookRepository.save(book)));
    }

    @Override
    public ApiResult<BookDTO> updateBook(Integer id, AddBookDTO addBookDTO) {
        Book book = bookRepository.findById(id).orElseThrow(() -> RestException.restThrow("Book not found with this id = " + id, HttpStatus.NOT_FOUND));
        book.setName(addBookDTO.getName());
        book.setAuthor(addBookDTO.getAuthor());
        book.setPageCount(addBookDTO.getPageCount());
        return ApiResult.success(BookDTO.DTO(bookRepository.save(book)));
    }

    @Override
    public void deleteBook(Integer id) {
        ;
        bookRepository.delete(
                bookRepository
                        .findById(id)
                        .orElseThrow(
                                ()->RestException.restThrow("Book not found with this id = " + id, HttpStatus.NOT_FOUND)));
    }
}
