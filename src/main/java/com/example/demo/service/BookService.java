package com.example.demo.service;

import com.example.demo.DTO.AddBookDTO;
import com.example.demo.DTO.BookDTO;
import com.example.demo.apiResult.ApiResult;

import java.util.List;

public interface BookService {
    ApiResult<BookDTO> getById(Integer id);
    ApiResult<List<BookDTO>> getAll();
    ApiResult<BookDTO> addBook(AddBookDTO bookDTO);
    ApiResult<BookDTO> updateBook(Integer id, AddBookDTO addBookDTO);
    void deleteBook(Integer id);
}
