package com.example.demo.service;

import com.example.demo.DTO.UpdateUserDTO;
import com.example.demo.DTO.UserDTO;
import com.example.demo.apiResult.ApiResult;
import com.example.demo.entity.enums.Role;

public interface UserService {
    void delete(Integer id);
    ApiResult<UserDTO> editRole (Integer id, Role role);

    ApiResult<UserDTO> update (Integer id, UpdateUserDTO userDTO);
}
