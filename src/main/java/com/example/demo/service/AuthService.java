package com.example.demo.service;

import com.example.demo.DTO.SignDTO;
import com.example.demo.DTO.TokenDTO;
import com.example.demo.apiResult.ApiResult;
import org.springframework.security.core.userdetails.UserDetailsService;


public interface AuthService extends UserDetailsService {
    ApiResult<Boolean> signUp(SignDTO signDTO);


    ApiResult<TokenDTO> signIn(SignDTO signDTO);

    ApiResult<TokenDTO> refreshToken(String accessToken, String refreshToken);
}
