package com.example.demo.service;

import com.example.demo.DTO.UpdateUserDTO;
import com.example.demo.DTO.UserDTO;
import com.example.demo.apiResult.ApiResult;
import com.example.demo.entity.User;
import com.example.demo.entity.enums.Role;
import com.example.demo.exceptions.RestException;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;


    @Override
    public void delete(Integer id) {

        userRepository.deleteById(id);

    }

    @Override
    public ApiResult<UserDTO> editRole(Integer id, Role role) {
        User user = userRepository.findById(id).orElseThrow(() ->
                RestException.restThrow("User is not found with this id =" + id, HttpStatus.NOT_FOUND));
        user.setRole(role);
        return ApiResult.success(UserDTO.OTD(userRepository.save(user)));
    }

    @Override
    public ApiResult<UserDTO> update(Integer id, UpdateUserDTO userDTO) {
        User user = userRepository.findById(id).orElseThrow(()->RestException.restThrow("User not found with this id = " + id,HttpStatus.NOT_FOUND));
        user.setEmail(userDTO.getEmail());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        return ApiResult.success(UserDTO.OTD(userRepository.save(user)));
    }
}
