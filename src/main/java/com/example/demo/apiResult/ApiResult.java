package com.example.demo.apiResult;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class ApiResult<T> {
    private boolean success;
    private String msg;
    private  T data;

    private List<Error> errors;

    public static<T> ApiResult<T> success(String msg,T data){
        ApiResult<T> result = success(data);
        result.msg = msg;
        return result;
    }

    public static <T> ApiResult<T> success(T data){
        ApiResult<T> result = ApiResult.success();
        result.data = data;
        return result;
    }

    public static <T> ApiResult<T> success(){
        ApiResult<T> result = new ApiResult<>();
        result.success = true;
        return result;
    }

    public static ApiResult<List<Error>> fail(List<Error> errors){
        ApiResult<List<Error>> result = new ApiResult<>();
        result.success = false;
        result.data = errors;
        return result;
    }

    public static ApiResult<List<Error>> fail(String msg, Integer code){
        ApiResult<List<Error>> result = new ApiResult<>();
        result.success = false;
        result.errors = List.of(new Error(msg, code));
        return result;
    }
}
