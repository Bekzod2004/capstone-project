package com.example.demo.entity.enums;

import com.example.demo.exceptions.RestException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;

import java.util.Objects;

public enum Role implements GrantedAuthority {
    ADMIN,
    SUPER_ADMIN,
    USER;

    @Override
    public String getAuthority() {
        return this.name();
    }

    public static Role getRole(String role){
        if(Objects.equals(role,"ADMIN"))
            return ADMIN;
        else if (Objects.equals(role,"SUPER_ADMIN"))
            return SUPER_ADMIN;
        else if (Objects.equals(role,"USER"))
            return USER;
        else throw RestException.restThrow("Given Role is not Valid", HttpStatus.BAD_REQUEST);
    }
}
