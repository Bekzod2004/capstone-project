package com.example.demo.DTO;

import com.example.demo.entity.Book;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookDTO {
    private Integer id;
    private String name;
    private String author;
    private Integer pageCount;

    public static BookDTO DTO(Book book){
        BookDTO book1 = new BookDTO();
        book1.setId(book.getId());
        book1.setName(book.getName());
        book1.setAuthor(book.getAuthor());
        book1.setPageCount(book.getPageCount());
        return book1;
    }
}
