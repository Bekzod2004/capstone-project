package com.example.demo.controller;

import com.example.demo.DTO.RoleDTO;
import com.example.demo.DTO.UpdateUserDTO;
import com.example.demo.DTO.UserDTO;
import com.example.demo.apiResult.ApiResult;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RequestMapping(UserController.USER)
public interface UserController {
    String USER = "api/user";
    String DELETE = "/delete/{id}";
    String EDIT_USER_ROLE = "/edit-role/{id}";
    String EDIT_USER = "/edit/{id}";

    @DeleteMapping(DELETE)
    void delete(@PathVariable Integer id);

    @PreAuthorize(value = "hasAnyAuthority('SUPER_ADMIN')")
    @PutMapping(EDIT_USER_ROLE)
    ApiResult<UserDTO> editRole(@PathVariable Integer id, @RequestBody RoleDTO role);

    @PutMapping(EDIT_USER)
    ApiResult<UserDTO> edit(@PathVariable Integer id, UpdateUserDTO userDTO);
}
