package com.example.demo.controller;

import com.example.demo.DTO.AddBookDTO;
import com.example.demo.DTO.BookDTO;
import com.example.demo.apiResult.ApiResult;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(BookController.BOOK)
public interface BookController {

    String BOOK = "/api/book";
    String GET_BY_ID = "/by-id/{id}";
    String GET_ALL = "/all";
    String UPDATE = "/update/{id}";
    String DELETE = "/{id}";

    @PreAuthorize(value = "hasAnyAuthority('USER','ADMIN','SUPER_ADMIN')")
    @GetMapping(GET_BY_ID)
    ApiResult<BookDTO> getById(@PathVariable Integer id);

    @GetMapping(GET_ALL)
    ApiResult<List<BookDTO>> getAll();

    @PreAuthorize(value = "hasAnyAuthority('ADMIN','SUPER_ADMIN')")
    @PostMapping
    ApiResult<BookDTO> add(@RequestBody AddBookDTO addBookDTO);

    @PreAuthorize(value = "hasAnyAuthority('ADMIN','SUPER_ADMIN')")
    @PutMapping(UPDATE)
    ApiResult<BookDTO> update(@PathVariable Integer id, @RequestBody AddBookDTO addBookDTO);

    @PreAuthorize(value = "hasAnyAuthority('SUPER_ADMIN')")
    @DeleteMapping(DELETE)
    void  delete(@PathVariable Integer id);
}
