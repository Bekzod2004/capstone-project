package com.example.demo.controller.implementations;

import com.example.demo.DTO.AddBookDTO;
import com.example.demo.DTO.BookDTO;
import com.example.demo.apiResult.ApiResult;
import com.example.demo.controller.BookController;
import com.example.demo.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class BookControllerImpl implements BookController {

    private final BookService service;

    @Override
    public ApiResult<BookDTO> getById(Integer id) {
        return service.getById(id);
    }

    @Override
    public ApiResult<List<BookDTO>> getAll() {
        return service.getAll();
    }

    @Override
    public ApiResult<BookDTO> add(AddBookDTO addBookDTO) {
        return service.addBook(addBookDTO);
    }

    @Override
    public ApiResult<BookDTO> update(Integer id, AddBookDTO addBookDTO) {
        return service.updateBook(id,addBookDTO);
    }

    @Override
    public void delete(Integer id) {
        service.deleteBook(id);
    }
}
