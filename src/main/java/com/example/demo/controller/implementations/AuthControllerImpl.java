package com.example.demo.controller.implementations;

import com.example.demo.DTO.SignDTO;
import com.example.demo.DTO.TokenDTO;
import com.example.demo.apiResult.ApiResult;
import com.example.demo.controller.AuthController;
import com.example.demo.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@RequiredArgsConstructor
@RestController
@Slf4j
public class AuthControllerImpl implements AuthController {
    private final AuthService authService;

    public ApiResult<Boolean> signUp(@RequestBody @Valid SignDTO signDTO) {
        log.info("SIgn up method entered: {}", signDTO);
        ApiResult<Boolean> apiResult = authService.signUp(signDTO);
        log.info("SIgn up method exited: {}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<TokenDTO> signIn(SignDTO signDTO) {
        log.info("SIgn in method entered: {}", signDTO);
        ApiResult<TokenDTO> apiResult = authService.signIn(signDTO);
        log.info("SIgn in method exited: {}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<TokenDTO> refreshToken(String accessToken, String refreshToken) {
        return authService.refreshToken(accessToken,refreshToken);
    }
}
