package com.example.demo.controller.implementations;

import com.example.demo.DTO.RoleDTO;
import com.example.demo.DTO.UpdateUserDTO;
import com.example.demo.DTO.UserDTO;
import com.example.demo.apiResult.ApiResult;
import com.example.demo.controller.UserController;
import com.example.demo.entity.enums.Role;
import com.example.demo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserControllerImpl implements UserController {
    private final UserService service;

    @Override
    public void delete(Integer id) {
        service.delete(id);
    }

    @PreAuthorize(value = "hasAnyAuthority('SUPER_ADMIN')")
    @Override
    public ApiResult<UserDTO> editRole(Integer id, RoleDTO role) {
        return service.editRole(id, Role.getRole(role.getRole()) );
    }

    @Override
    public ApiResult<UserDTO> edit(Integer id, UpdateUserDTO userDTO) {
        return service.update(id,userDTO);
    }
}
