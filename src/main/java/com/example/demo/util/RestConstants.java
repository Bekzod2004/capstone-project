package com.example.demo.util;

import com.example.demo.controller.AuthController;
import com.example.demo.controller.BookController;
import com.example.demo.controller.UserController;

public interface RestConstants {
    String AUTHENTICATION_HEADER = "Authorization";

    String[] OPEN_PAGES = {
            AuthController.AUTH_CONTROLLER_BASE_PATH + "/*",
            BookController.BOOK + BookController.GET_ALL
    };

    String[] PAGES_FOR_USER = {
            AuthController.AUTH_CONTROLLER_BASE_PATH + "/*",
            BookController.BOOK + BookController.GET_ALL,
            BookController.BOOK + BookController.GET_BY_ID

    };

    String[] PAGES_FOR_ADMIN = {
            AuthController.AUTH_CONTROLLER_BASE_PATH + "/*",
            BookController.BOOK + BookController.GET_ALL,
            BookController.BOOK + BookController.GET_BY_ID,
            BookController.BOOK + BookController.UPDATE,
            BookController.BOOK

    };
    String[] PAGES_FOR_SUPER_ADMIN = {
            AuthController.AUTH_CONTROLLER_BASE_PATH + "/****",
            BookController.BOOK + "/****",
            UserController.USER + "/****"
    };
}
